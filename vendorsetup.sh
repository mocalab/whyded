# Override host metadata to make builds more reproducible and avoid leaking info
export BUILD_USERNAME=yuki
export BUILD_HOSTNAME=derpnest
